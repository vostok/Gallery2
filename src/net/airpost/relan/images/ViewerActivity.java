/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.Manifest;
import android.app.WallpaperManager;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class ViewerActivity extends PermissionActivity {

    private Uri mUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        setActionBar((AdvancedToolbar) findViewById(R.id.toolbar));

        Intent intent = getIntent();
        if (intent.getAction().equals(Intent.ACTION_VIEW)) {
            mUri = intent.getData();
            if (mUri == null) {
                // View the whole gallery
                startActivity(ImagesTrampolineActivity.makeTrampolineIntent());
                finish();
            } else {
                // View a particular image; can be "content:" or "file:" URI
                obtainPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    protected void onPermissionGranted() {
        if (mUri.getScheme().equals("file")) {
            // Convert "file:" URI to "content:" URI
            MediaScannerConnection.scanFile(this,
                    new String[] { mUri.getPath() }, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            mUri = uri; // Now it's "content:" URI
                            doTasks();
                        }
                    });
        } else {
            doTasks();
        }
    }

    private void doTasks() {
        new SetImageNameTask(this).execute(mUri);
        new LoadImageTask(this).execute(mUri);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.viewer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_share:
            if (mUri != null) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_STREAM, mUri);
                intent.setType("image/*");
                startActivity(Intent.createChooser(intent, null));
            }
            return true;

        case R.id.action_wallpaper:
            if (mUri != null) {
                startActivityForResult(WallpaperManager
                        .getInstance(this)
                        .getCropAndSetWallpaperIntent(mUri), 42);
            }
            return true;

        default:
            return super.onOptionsItemSelected(item);
        }
    }

}
