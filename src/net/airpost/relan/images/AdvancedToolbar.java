/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toolbar;


public class AdvancedToolbar extends Toolbar {

    final Resources mResources;
    final WindowManager mWM;
    final AnimatorListenerAdapter mGoneOnAnimationEnd;

    public AdvancedToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);

        mResources = getResources();
        mWM = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        mGoneOnAnimationEnd = new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    setVisibility(View.GONE);
                }};

        // Handle exiting from immersive mode by a swipe from a screen border
        setOnSystemUiVisibilityChangeListener(new
            View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int newVisibility) {
                        // SYSTEM_UI_FLAG_IMMERSIVE never appears here (?)
                        if (newVisibility == 0) {
                            show();
                        }
                    }});
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Apply a margin to the toolbar; needed to avoid overlapping with
        // navigation bar on landscape-orientated phones
        if (mResources.getBoolean(R.bool.side_navigation_bar)) {
            int navigationBarSize =
                    (int) mResources.getDimension(R.dimen.navigation_bar_size);
            widthMeasureSpec -= navigationBarSize;
            setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
            setTranslationX(leftNavigationBar() ? navigationBarSize : 0);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private boolean leftNavigationBar() {
        // Android 7.1 phones in landscape orientation can have navigation bar
        // either on the left or on the right; previous versions always put
        // navigation bar on the right
        return Build.VERSION.SDK_INT >= 25
            && mWM.getDefaultDisplay().getRotation() == Surface.ROTATION_270;
    }

    public void hide() {
        animate().scaleY(0.0f).setListener(mGoneOnAnimationEnd);
    }

    public void show() {
        setVisibility(View.VISIBLE);
        animate().scaleY(1.0f).setListener(null);
    }

}
