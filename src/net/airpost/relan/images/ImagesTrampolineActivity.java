/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;

public class ImagesTrampolineActivity extends TrampolineActivity {

    @Override
    protected Intent getTrampolineIntent() {
        return makeTrampolineIntent();
    }

    static public Intent makeTrampolineIntent() {
        Intent intent = new TrampolineIntent();
        intent.setData(new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority("com.android.providers.media.documents")
                .appendPath("root").appendPath("images_root").build());
        return intent;
    }

}
