/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.View;
import java.lang.ref.WeakReference;
import uk.co.senab.photoview.PhotoViewAttacher.OnPhotoTapListener;


class PhotoTapListener implements OnPhotoTapListener {

    private final WeakReference<View> mDecorRef;
    private final WeakReference<AdvancedToolbar> mToolbarRef;
    private final int SYSTEM_UI_SHOW = View.SYSTEM_UI_FLAG_IMMERSIVE;
    private final int SYSTEM_UI_HIDE = View.SYSTEM_UI_FLAG_IMMERSIVE
                                     | View.SYSTEM_UI_FLAG_FULLSCREEN
                                     | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

    PhotoTapListener(Activity activity) {
        mDecorRef = new WeakReference(activity.getWindow().getDecorView());
        mToolbarRef = new WeakReference(activity.findViewById(R.id.toolbar));

        // Immersive flag allows guestures in full screen mode (i.e. when
        // navigation and status bars are hidden)
        mDecorRef.get().setSystemUiVisibility(SYSTEM_UI_SHOW);
    }

    @Override
    public void onPhotoTap(View view, float x, float y) {
        toggleSystemUi();
    }

    @Override
    public void onOutsidePhotoTap() {
        toggleSystemUi();
    }

    private void toggleSystemUi() {
        View decor = mDecorRef.get();
        AdvancedToolbar toolbar = mToolbarRef.get();
        if (decor != null && toolbar != null) {
            if (decor.getSystemUiVisibility() == SYSTEM_UI_SHOW) {
                decor.setSystemUiVisibility(SYSTEM_UI_HIDE);
                toolbar.hide();
            } else {
                decor.setSystemUiVisibility(SYSTEM_UI_SHOW);
                toolbar.show();
            }
        }
    }

}
