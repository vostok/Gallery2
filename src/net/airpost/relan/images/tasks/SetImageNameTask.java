/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;


class SetImageNameTask extends ActivityTask<Uri, Void, String> {

    SetImageNameTask(Activity activity) {
        super(activity);
    }

    @Override
    protected String doInBackground(Uri... params) {
        String name = null;
        Cursor cursor = mResolver.query(params[0], null, null, null, null);
        if (cursor != null) {
            // Get name from content resolver
            if (cursor.moveToFirst()) {
                int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                name = cursor.getString(nameIndex);
            }
            cursor.close();
        }
        return name;
    }

    @Override
    protected void onPostExecute(String name) {
        Activity activity = mActivityRef.get();
        if (activity != null) {
            activity.getActionBar().setTitle(name);
        }
    }

}
