/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import uk.co.senab.photoview.PhotoViewAttacher;


class LoadImageTask extends ActivityTask<Uri, Void, Bitmap> {

    LoadImageTask(Activity activity) {
        super(activity);
    }

    @Override
    protected Bitmap doInBackground(Uri... params) {
        decode(params[0]);
        return BitmapCache.INSTANCE.get(params[0]);
    }

    private boolean decode(Uri uri) {
        return BitmapCache.INSTANCE.get(uri) != null
            || decode(new DecoderRaster(), uri)
            || decode(new DecoderVector(), uri);
    }

    private boolean decode(Decoder decoder, Uri uri) {
        // Try to decode an image (can be very slow)
        Bitmap result = decoder.decode(mResolver, uri);
        if (result != null) {
            // Save bitmap for reuse (e.g. on configuration change)
            BitmapCache.INSTANCE.set(uri, result);
            return true;
        }
        return false;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        Activity activity = mActivityRef.get();
        if (activity == null) {
            return;
        }
        if (bitmap != null) {
            ImageView imageView = (ImageView) activity.findViewById(R.id.image);
            imageView.setImageBitmap(bitmap);
            PhotoViewAttacher attacher = new PhotoViewAttacher(imageView);
            attacher.setOnPhotoTapListener(new PhotoTapListener(activity));
        } else {
            activity.findViewById(R.id.error).setVisibility(View.VISIBLE);
        }
        activity.findViewById(R.id.progress).setVisibility(View.GONE);
    }

    private static enum BitmapCache {

        INSTANCE;
        private Uri mId;
        private Bitmap mBitmap;

        public Bitmap get(Uri id) {
            return id.equals(mId) ? mBitmap : null;
        }

        public void set(Uri id, Bitmap bitmap) {
            mId = id;
            mBitmap = bitmap;
        }

    }

}
