/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.io.InputStream;
import java.io.IOException;
import java.lang.Math;


class DecoderRaster implements Decoder {

    private static int getLargestDimension(ContentResolver resolver, Uri uri) {
        try (InputStream stream = resolver.openInputStream(uri)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(stream, null, options);
            return Math.max(options.outWidth, options.outHeight);
        } catch (IOException e) {
            return 0;
        }
    }

    private static int getDownscaleFactor(int dimen) {
        int factor = 1;
        while (dimen / factor > DIMENSION_MAX) {
            factor *= 2;
        }
        return factor;
    }

    public Bitmap decode(ContentResolver resolver, Uri uri) {
        int dsf = getDownscaleFactor(getLargestDimension(resolver, uri));
        try (InputStream stream = resolver.openInputStream(uri)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = dsf;
            return BitmapFactory.decodeStream(stream, null, options);
        } catch (IOException e) {
            return null;
        }
    }

}
