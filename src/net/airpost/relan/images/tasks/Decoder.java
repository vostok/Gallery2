/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;


interface Decoder {

    // If image width or height exceeds this value it will be downscaled.
    // Otherwise we will not be able to display it at all due to
    // OpenGLRenderer texture size limit and memory constraints.
    static final int DIMENSION_MAX = 4096;

    Bitmap decode(ContentResolver resolver, Uri uri);

}
