/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.content.Intent;
import android.os.Build;

class TrampolineIntent extends Intent {

    public TrampolineIntent() {
        super(getBrowseActionString());
        addCategory(Intent.CATEGORY_DEFAULT);
    }

    private static String getBrowseActionString() {
        if (Build.VERSION.SDK_INT <= 23) {          // Marshmallow
            return "android.provider.action.BROWSE_DOCUMENT_ROOT";
        } else if (Build.VERSION.SDK_INT <= 24) {   // Nougat
            return "android.provider.action.BROWSE";
        } else {                                    // Oreo
            return "android.intent.action.VIEW";
        }
    }

}
