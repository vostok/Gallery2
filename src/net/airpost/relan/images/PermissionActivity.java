/*
 * Vostok Image Viewer
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.images;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;


abstract public class PermissionActivity extends Activity {

    private static int REQUEST_CODE = 42;

    protected void obtainPermission(String perm) {
        if (checkSelfPermission(perm) == PackageManager.PERMISSION_GRANTED) {
            // Permission has already been granted
            onPermissionGranted();
        } else {
            requestPermissions(new String[] { perm }, REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
            String[] permissions, int[] results) {
        if (requestCode == REQUEST_CODE
                && results.length > 0
                && results[0] == PackageManager.PERMISSION_GRANTED) {
            onPermissionGranted();
        } else {
            onPermissionDenied();
        }
    }

    protected void onPermissionGranted() {
    }

    protected void onPermissionDenied() {
    }

}
