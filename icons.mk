icons = ic_launcher_images ic_launcher_files
mipmap = res/mipmap
densities = mdpi hdpi xhdpi xxhdpi xxxhdpi

all : $(foreach icon, $(icons), \
          $(foreach dpi, $(densities), \
              $(mipmap)-$(dpi)/$(icon).png))

$(mipmap)-mdpi/%.png : %.svg
	inkscape $< -e $@ -w 48

$(mipmap)-hdpi/%.png : %.svg
	inkscape $< -e $@ -w 72

$(mipmap)-xhdpi/%.png : %.svg
	inkscape $< -e $@ -w 96

$(mipmap)-xxhdpi/%.png : %.svg
	inkscape $< -e $@ -w 144

$(mipmap)-xxxhdpi/%.png : %.svg
	inkscape $< -e $@ -w 192

.PHONY : all
